import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios';

axios.defaults.baseURL = "http://localhost:3356/api/"

createApp(App).use(router).mount('#app')
