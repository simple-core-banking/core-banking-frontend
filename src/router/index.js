import { createRouter, createWebHistory } from 'vue-router'
import CifList from '../views/CifList.vue'
import CreateCif from '../views/CreateCif.vue'
import UpdateCif from '../views/UpdateCif.vue'
import CifAddressList from '../views/CifAddressList.vue'
import CreateCifAddress from '../views/CreateCifAddress.vue'
import CifFamilyList from '../views/CifFamilyList.vue'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/cif',
    name: 'CifList',
    component: CifList
  },
  {
    path: '/create',
    name: 'CreateCif',
    component: CreateCif
  },
  {
    path: '/update/:idCard',
    name: 'UpdateCif',
    component: UpdateCif
  },
  {
    path: '/cifaddress',
    name: 'CifAddressList',
    component: CifAddressList
  },
  {
    path: '/createaddress',
    name: 'CreateCifAddress',
    component: CreateCifAddress
  },
  {
    path: '/ciffamily',
    name: 'CifFamilyList',
    component: CifFamilyList
  },
 
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
